# Antennen Design

**Varinate 1: Whip antenna** 

I used the KNX cable for that

![Antenna-Icon](pic/Antenne.png)

Solder point for antenna cable (red arrow)

![AntennaPos-Icon](pic/AntennePos.jpg)

----------------------------------------------------------

**Varinate 2: SMA 868 MHz Antenna** 

_Important: SMA with Pin (red arrow)_

![Antenna2-Icon](pic/AntenneSMA.png)



